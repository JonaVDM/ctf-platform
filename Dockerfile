FROM node:18-alpine AS frontend-builder
WORKDIR /app
RUN corepack enable
COPY ui/package.json .
COPY ui/pnpm-lock.yaml .
RUN pnpm install --frozen-lockfile
COPY ui .
RUN npm run build

FROM golang:1.21.6-alpine AS go-builder
WORKDIR /app
COPY go.* ./
RUN go mod download
COPY . .
COPY --from=frontend-builder /app/build /app/ui/build
RUN go build .

FROM alpine
WORKDIR /app
COPY --from=go-builder /app/ctf-platform /app/ctf-platform
ENV GIN_MODE=release
EXPOSE 8090
CMD ["./ctf-platform", "serve", "--http=0.0.0.0:8090"]

package hooks

import (
	"net/http"

	"github.com/labstack/echo/v5"
	"github.com/pocketbase/pocketbase/apis"
	"github.com/pocketbase/pocketbase/core"
)

func (h *Hooks) registerDocker() {
	h.pb.OnBeforeServe().Add(func(e *core.ServeEvent) error {
		group := e.Router.Group("/api/docker", apis.ActivityLogger(h.pb), apis.RequireRecordAuth())

		group.GET("/containers", func(c echo.Context) error {
			return c.JSON(http.StatusOK, []string{})
		})

		return nil
	})
}

package hooks

import (
	"github.com/pocketbase/pocketbase/core"
)

type Hooks struct {
	pb core.App
}

func MustRegister(app core.App) {
	hook := Hooks{
		pb: app,
	}

	hook.registerDocker()
}

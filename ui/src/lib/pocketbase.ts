import { writable } from 'svelte/store';
import PocketBase from 'pocketbase'

export const pb = new PocketBase('/');

export const currentUser = writable(pb.authStore.model);

pb.authStore.onChange(() => {
  currentUser.set(pb.authStore.model);
});

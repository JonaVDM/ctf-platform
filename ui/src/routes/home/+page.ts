import { pb } from "$lib/pocketbase"
import type { PageLoad } from "./$types"

export const load: PageLoad = async ({ fetch }) => {
  return {
    // test: await pb.collection('test').getFullList({ fetch }),
    test: await pb.send('/api/docker/containers', { fetch }),
  }
}
